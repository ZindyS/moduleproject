package com.example.moduleproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.example.mylibrary.LoginScreen
import com.example.mylibrary.smt

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Handler().postDelayed({
            smt = Intent(this, SecondActivity::class.java)
            val intent = Intent(this, LoginScreen::class.java)
            startActivity(intent)
        }, 2000)
    }
}