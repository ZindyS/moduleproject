package com.example.mylibrary

import android.content.DialogInterface
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

lateinit var smt : Intent

fun checkEmail(str: String) : Boolean {
    var sob=false
    var cnt=-2
    for (i in str) {
        if (i=='@') {
            sob=true
        } else if (i=='.' && sob) {
            cnt++
        } else if (i in 'a'..'z') {}
        else if (!sob && (i in 'A'..'Z' || i in '0'..'9')) {}
        else if (i=='.') {}
        else {
            sob=false
            break
        }


        if (cnt!=-2) { cnt++ }
    }
    return (sob && cnt>=2 && cnt<=3)
}

class LoginScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_screen)
    }

    fun onLoginClicked(v: View) {
        val loginStr = findViewById<EditText>(R.id.editTextTextPersonName).text.toString()
        val passwordStr = findViewById<EditText>(R.id.editTextTextPersonName2).text.toString()
        if (loginStr == "" || passwordStr == "") {
            AlertDialog.Builder(this)
                .setTitle("Ошибка")
                .setMessage("Поля не должны быть пустыми!")
                .setNegativeButton("OK") { dialogInterface: DialogInterface, i: Int -> }
                .show()
            return
        }
        if (!checkEmail(loginStr)) {
            AlertDialog.Builder(this)
                .setTitle("Ошибка")
                .setMessage("Проверьте правильность Email'а!")
                .setNegativeButton("OK") { dialogInterface: DialogInterface, i: Int -> }
                .show()
            return
        }
        val retrofit = Retrofit.Builder()
            .baseUrl("http://mskko2021.mad.hakta.pro/api/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        val api = retrofit.create(SomeAPI::class.java)
        api.login(UserBody(loginStr, passwordStr)).enqueue(object : Callback<UserResponse> {
            override fun onResponse(
                call: Call<UserResponse>,
                response: Response<UserResponse>
            ) {
                if (response.isSuccessful) {
                    Log.d("errror", "Norm")
//                    AlertDialog.Builder(this@LoginScreen)
//                        .setTitle("Norm")
//                        .setMessage("Norm")
//                        .setNegativeButton("OK") { dialogInterface: DialogInterface, i: Int -> }
//                        .show()
                    startActivity(smt)
                    //Toast.makeText(this@LoginScreen, "Norm", Toast.LENGTH_SHORT).show()
                } else {
                    Log.d("errror1", response.message().toString())
                    if (response.message()=="") {
                        AlertDialog.Builder(this@LoginScreen)
                            .setTitle("Ошибка")
                            .setMessage("Неправильный логин или пароль")
                            .setNegativeButton("OK") { dialogInterface: DialogInterface, i: Int -> }
                            .show()
                        return
                    }
                    AlertDialog.Builder(this@LoginScreen)
                        .setTitle("Ошибка")
                        .setMessage(response.message())
                        .setNegativeButton("OK") { dialogInterface: DialogInterface, i: Int -> }
                        .show()
                    //Toast.makeText(this@LoginScreen, response.message(), Toast.LENGTH_SHORT).show()
                }
            }

            override fun onFailure(call: Call<UserResponse>, t: Throwable) {
                Log.d("errror2", t.message.toString())
                AlertDialog.Builder(this@LoginScreen)
                    .setTitle("Ошибка")
                    .setMessage(t.message)
                    .setNegativeButton("OK") { dialogInterface: DialogInterface, i: Int -> }
                    .show()
                //Toast.makeText(this@LoginScreen, t.message, Toast.LENGTH_SHORT).show()
            }
        })
    }
}