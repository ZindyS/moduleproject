package com.example.mylibrary

import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.POST

interface SomeAPI {
    @POST("user/login")
    fun login(@Body body : UserBody) : Call<UserResponse>
}