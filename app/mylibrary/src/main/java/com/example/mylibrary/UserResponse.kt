package com.example.mylibrary

data class UserResponse(
    val avatar: String,
    val email: String,
    val id: String,
    val nickName: String,
    val token: String,
    val error: String,
    val success: Boolean
)